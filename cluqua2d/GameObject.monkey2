
Namespace cluqua2d


#Rem monkeydoc Base class for all entities in cluqua2d scenes.

See Also: Component.
#End
Class GameObject
	
	#Rem monkeydoc Callback to catch game object destruction moment.
	#End
	Field OnDestroy:Void()
	
	#Rem monkeydoc Callback to catch game object SetActive() moment.
	#End
	Field OnActiveChanged:Void()
	
	#Rem monkeydoc The name of the object.
	
	Names must be unique inside of the same transform (otherwise name will be changed automatically by adding indexed suffix like 'ObjName(4)').
	#End
	Property name:String()
		Return _name
	Setter( value:String )
		' check is name unique
		If transform And transform.parent
			Local index:=1
			Local sourceValue:=value
			Repeat
				Local unique:=True
				For Local t:=Eachin transform.parent.children
					If t.name=value
						unique=False
						Exit
					Endif
				Next
				If unique Exit
				value=sourceValue+"("+index+")"
				index+=1
			Forever
		Endif
		_name=value
	End
	
	#Rem monkeydoc The tag of game object.
	Tags must be registered before using them.
	#End
	Property tag:String()
		Return _tag
	Setter( value:String )
		If _tag Then TagManagerBridge_Go.RemoveObject( Self )
		_tag=value
		If _tag Then TagManagerBridge_Go.AddObject( Self )
	End
	
	#Rem monkeydoc The local active state of this GameObject. (Read Only)
	
	This returns the local active state of this GameObject, which is set using GameObject.SetActive.
	Note that a GameObject may be inactive because a parent is not active, even if this returns true.
	This state will then be used once all parents are active.
	Use GameObject.activeInHierarchy if you want to check if the GameObject is actually treated as active in the scene.
	
	See Also: GameObject.SetActive, GameObject.activeInHierarchy.
	#End
	Property activeSelf:Bool()
		Return _activeSelf
	End
	
	#Rem monkeydoc Is the GameObject active in the scene?
	
	This lets you know if a gameObject is active in the game.
	That is the case if its GameObject.activeSelf property is enabled, as well as that of all it's parents.
	
	See Also: GameObject.SetActive, GameObject.activeSelf.
	#End
	Property activeInHierarchy:Bool()
		Return _activeSelf And _activeInHierarchy
	End
	
	#Rem monkeydoc The layer the game object is in. A layer is in the range [0...31].
	#End
	Property layer:Int()
		Return _layer
	Setter( value:Int )
		LayerManagerBridge_Go.Assign( Self,value )
		_layer=value
	End
	
	#Rem monkeydoc The Transform attached to this GameObject.
	#End
	Property transform:Transform()
		Return _transform
	End
	
	#Rem monkeydoc Scene that the GameObject is part of.
	#End
	Property scene:Scene()
		Return _scene
	End
	
	#Rem monkeydoc Creates a new game object, named name.
	Use GameObject.Create instead of New to get objects from object pool.
	#End
	Method New( name:String )
		_name=name
		_transform=AddComponent<Transform>()
	End
	
	#Rem monkeydoc Is this game object tagged with tag?
	#End
	Method CompareTag:Bool( tag:String )
		Return (Self.tag=tag)
	End
	
	#Rem monkeydoc Activates/Deactivates the GameObject.
	
	Note that a GameObject may be inactive because a parent is not active.
	In that case, calling SetActive() will not activate it, but only set the local state of the GameObject, which can be checked using GameObject.activeSelf.
	This state will then be used once all parents are active.
	
	Making a GameObject inactive will disable every component, turning off any attached renderers, scripts, etc...
	Any scripts that you have attached to the GameObject will no longer have Update() called, for example.
	#End
	Method SetActive( value:Bool )
	
		If value=_activeSelf Return
		
		_activeSelf=value
		OnActiveChanged()
		
		UpdateActiveInHierarchy( Self )
	End
	
	#Rem monkeydoc Adds a component class of type T to the game object.
	#End
	Method AddComponent<T>:T() Where T Extends Component
		
		Local comp:=ComponentBridge_Go.NewComponent<T>()
		
		Local name:=Typeof( comp ).Name
		
		Local items:=_components[name]
		
		Local unique:=comp.isUnique
		If unique
			Assert( items=Null Or items.Length=1,"Allowed only one instance of '"+name+"' component to be attached!" )
		Endif
		
		If items=Null
			items=New Stack<Component>
			_components.Add( name,items )
		Endif
		
		items.Add( comp )
		
		ComponentBridge_Go.SetGameObject( comp,Self )
		
		ScriptExecutorBridge_Go.GrabComponent( comp )
		
		Return comp
	End
	
	#Rem monkeydoc Returns the component of T type if the game object has one attached, null if it doesn't.
	
	GetComponent is the primary way of accessing other components.
	#End
	Method GetComponent<T>:T( c:T=Null ) Where T Extends Component
	
		Local name:=Typeof( c ).Name
		Local items:=_components[name]
		If items=Null Or items.Empty Return Null
		Return Cast<T>( items[0] )
	End
	
	#Rem monkeydoc Returns array with all components of T type in the GameObject, or null if nothing found.
	#End
	Method GetAllComponents<T>:T[]( c:T=Null ) Where T Extends Component
	
		Local name:=Typeof( c ).Name
		Local items:=_components[name]
		If items=Null Or items.Empty Return Null
		Local result:=New T[items.Count()]
		Local i:=0
		For Local it:=Eachin items
			result[i]=Cast<T>( it )
			i+=1
		Next
		Return result
	End
	
	#Rem monkeydoc Detach this component from game object and destroy it.
	#End
	Method RemoveComponent:Void( comp:Component )
		
		Local name:=Typeof( comp ).Name
		RemoveComponent( name )
	End
	
	#Rem monkeydoc Detach component of T type from game object and destroy it.
	#End
	Method RemoveComponent<T>:Void( c:T=Null ) Where T Extends Component
		
		Local name:=Typeof( c ).Name
		RemoveComponent( name )
	End
	
	#Rem monkeydoc Detach all components of T type from game object and destroy them.
	#End
	Method RemoveAllComponents<T>:Void( c:T=Null ) Where T Extends Component
	
		Local name:=Typeof( c ).Name
		RemoveAllComponents( name )
	End
	
	#Rem monkeydoc Set the parent of the game object.
	#End
	Method SetParent( parent:GameObject )
		_transform.parent=parent.transform
	End
	
	#Rem monkeydoc Set the parent of the game object.
	#End
	Method SetParent( parent:Transform )
		_transform.parent=parent
	End
	
	#Rem monkeydoc Destroy this game object. Destroy is delayed operation and occured at the end of current frame (after all Updates and LateUpdates).
	#End
	Method Destroy()
	
		Destroy( Self )
	End
	
	#Rem monkeydoc Creates a new game object, named name.
	
	Transform is always added to the GameObject that is being created.
	#End
	Function Create:GameObject( name:String )
	
		Local go:=PoolSystem.PopGameObject()
		go.name=name
		Return go
	End
	
	#Rem monkeydoc Finds one (active) GameObject by name and returns it.
	
	If no GameObject with name can be found, null is returned. If name contains a '/' character, it traverses the hierarchy like a path name.
	
	For performance reasons, it is recommended to not use this function every frame. Instead, cache the result in a member variable at startup or use GameObject.FindWithTag.
	
	Note: If you wish to find a child GameObject, it is often easier to use Transform.Find.
	#End
	Function Find:GameObject( name:String,activeOnly:Bool=True )
	
		Local scenes:=SceneManagerBridge_Go.GetAllScenes()
		For Local scene:=Eachin scenes
			Local root:=scene.root
			Local t:=root.Find( name )
			If t
				Local go:=t.gameObject
				If activeOnly And Not go.activeSelf Continue
				Return go
			Endif
		Next
		Return Null
	End
	
	#Rem monkeydoc Returns one (active) GameObject tagged tag. Returns null if no GameObject was found.
	
	Tags must be declared in the tag manager (Tags class) before using them.
	#End
	Function FindWithTag:GameObject( tag:String,activeOnly:Bool=True )
	
		Return TagManagerBridge_Go.FindObject( tag,activeOnly )
	End
	
	#Rem monkeydoc Returns an array of (active) GameObjects tagged tag. Returns empty array if no GameObject was found.
	
	Tags must be declared in the tag manager before using them.
	#End
	Function FindAllWithTag:GameObject[]( tag:String,activeOnly:Bool=True )
	
		Return TagManagerBridge_Go.FindAllObjects( tag,activeOnly )
	End
	
	#Rem monkeydoc Removes a gameobject or component.
	
	Destroy is delayed operation and occured at the end of current frame (after all Updates and LateUpdates).
	
	If obj is a Component it will remove the component from the GameObject and destroy it.
	If obj is a GameObject it will destroy the GameObject, all its components and all transform children of the GameObject.
	#End
	Function Destroy( obj:Object )
	
		Local comp:=Cast<Component>( obj )
		If comp
			comp.gameObject.RemoveComponent( comp )
			Return
		Endif
		Local go:=Cast<GameObject>( obj )
		If go
			go.SetActive( False )
			AddToDestroyList( go )
			Return
		Endif
	End
	
	
	Protected
	
	Field _scene:Scene
	Field _components:=New StringMap<Stack<Component>>
	
	Function DestroyObjects()
		
		If _listForDestroy.Empty Return
		
		For Local go:=Eachin _listForDestroy
			
			go.DestroyInternal()
			' need to push it into pool
		Next
		
		_listForDestroy.Clear()
		
	End
	
	
	Private

	Field _transform:Transform
	Field _layer:Int
	Field _name:String
	Field _activeSelf:=True,_activeInHierarchy:=True
	Field _tag:String
	
	Global _listForDestroy:=New Stack<GameObject>
	
	
	Method UpdateActiveInHierarchy( go:GameObject )
	
		Local trans:=go._transform
		If trans.childCount=0 Return
	
		Local active:=go.activeInHierarchy 'activeSelf and activeInHierarchy
	
		For Local t:=Eachin trans.children
	
			Local go:=t.gameObject
			If go._activeInHierarchy=active Continue 'already setted correct state
			go._activeInHierarchy=active
			UpdateActiveInHierarchy( go )
		Next
	End
	
	Function AddToDestroyList( go:GameObject )
	
		If _listForDestroy.Contains( go ) Return
		Print "AddToDestroyList"	
		_listForDestroy.Add( go )
	End
	
	Method DestroyInternal()
		
		For Local list:=Eachin _components.Values
			For Local c:=Eachin list
				If c=transform Continue 'don't destroy transform
				RemoveComponent( c )
				ComponentBridge_Go.OnDestroy( c )
			Next
		Next
		PoolSystem.PushGameObject( Self )
		OnDestroy()
	End
	
	Method RemoveComponent:Void( name:String )
	
		Local items:=_components[name]
		If items<>Null And Not items.Empty
			Local comp:=items[0]
			items.Remove( comp )
			ScriptExecutorBridge_Go.DeleteComponent( comp )
			ComponentBridge_Go.OnDestroy( comp )
		Endif
	End
	
	Method RemoveAllComponents:Void( name:String )
		
		Local items:=_components[name]
		If items<>Null
			While Not items.Empty
				Local comp:=items[0]
				items.Remove( comp )
				ScriptExecutorBridge_Go.DeleteComponent( comp )
			Wend
		Endif
	End
	
End



Private


Class ComponentBridge_Go Extends Component Abstract
	
	Function NewComponent<T>:T() Where T Extends Component
		Return New T
	End
	
	Function SetGameObject( comp:Component,go:GameObject )
		comp._gameObject=go
		comp.OnAttached()
	End
	
	Function OnDestroy( comp:Component )
		comp.OnDestroy()
	End
	
End


Class ScriptExecutorBridge_Go Extends ScriptExecutor Abstract

	Function GrabComponent( comp:Component )
		ScriptExecutor.GrabComponent( comp )
	End
	
	Function DeleteComponent( comp:Component )
		ScriptExecutor.DeleteComponent( comp )
	End
	
End


Class SceneManagerBridge_Go Extends SceneManager Abstract

	Function GetAllScenes:Stack<Scene>.Iterator()
		Return _scenes.All()
	End
	
End


Class TagManagerBridge_Go Extends TagManager Abstract

	Function AddObject( go:GameObject )
		TagManager.AddObject( go )
	End
	
	Function RemoveObject( go:GameObject )
		TagManager.RemoveObject( go )
	End
	
	Function FindObject:GameObject( tag:String,activeOnly:Bool=True )
		Return TagManager.FindObject( tag,activeOnly )
	End
	
	Function FindAllObjects:GameObject[]( tag:String,activeOnly:Bool=True )
		Return TagManager.FindAllObjects( tag,activeOnly )
	End
	
End


Class LayerManagerBridge_Go Extends LayerManager Abstract
	
	Function Assign:Void( go:GameObject,layer:Int=0 )
		LayerManager.Assign( go,layer )
	End
	
End
