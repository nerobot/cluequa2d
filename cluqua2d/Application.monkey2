
Namespace cluqua2d


#Rem monkeydoc The platform application is running. Returned by Application.platform.

|Value|Description
|:------------------|:-----------
|OSX		|In the player on macOS.
|Windows	|In the player on Windows.
|IPhone		|In the player on the iPhone.
|Android	|In the player on Android devices.
|Linux		|In the player on Linux.
|WebGL		|In the player on WebGL.
|Raspbian	|In the player on RaspberryPi.
#End
Enum RuntimePlatform
	OSX,
	Windows,
	Linux,
	IPhone,
	Android,
	WebGL,
	Raspbian
End


#Rem monkeydoc Global instance of cluqua2d application.
#End
Global Application:CluquaApplication


#Rem monkeydoc Access to application run-time data.

This class contains static methods for looking up information about and controlling the run-time data.
#End
Class CluquaApplication

	#Rem monkeydoc Callback that called at loading time.
	Use it to draw loading screen.
	
	Function Main()
	
		New CluquaApplication
		Application.Init( "cluqua2d - Big Wheel demo",800,800 )
	
		Application.RenderLoadingCallback=OnRenderLoading
	
		Application.Run()
	End
	
	Function OnRenderLoading( canvas:Canvas )
	
		'draw loading screen here
	End
	
	#End
	Field RenderLoadingCallback:Void( canvas:Canvas )
	
	#Rem monkeydoc This callback is entry point for user's code, you can create scene here.
	
	Function Main()
		
		New CluquaApplication
		Application.Init( "cluqua2d - Big Wheel demo",800,800 )
		
		Application.Initialized=OnInitializeApp
		
		Application.Run()
	End
	
	Function OnInitializeApp()
		
		'init scene...
		
		' all gameobject's tags must be registered before use
		Tags.RegisterTag( "MainCamera" )
		Tags.RegisterTag( "Base" )
		Tags.RegisterTag( "Cab" )
		Tags.RegisterTag( "TopCamera" )
		
		Local scene:=SceneManager.NewScene( "MainScene" )
		
		Local camObj:=CreateCamera( "Camera" )
		camObj.tag="MainCamera"
		camObj.transform.localPosition=New Vec2f( 0,Display.systemHeightHalf )
		scene.AddChild( camObj )
		
		'.....................
		
	End
	
	#End
	Field Initialized:Void()
	
	#Rem monkeydoc Contains the path to the game data folder (Read Only).
	
	The value depends on which platform you are running on:
	
	TODO: here is listing of paths for all supported targets
	#End
	Property dataPath:String()
		Return AssetsDir()
	End
	
	#Rem monkeydoc Returns the platform the game is running on (Read Only).
	
	Use this property if you need to do some platform dependent work.
	#End
	Property platform:RuntimePlatform()
		#If __TARGET__="windows"
		Return RuntimePlatform.Windows
		#Else If __TARGET__="macos"
		Return RuntimePlatform.OSX
		#Else If __TARGET__="linux"
		Return RuntimePlatform.Linux
		#Else If __TARGET__="android"
		Return RuntimePlatform.Android
		#Else If __TARGET__="ios"
		Return RuntimePlatform.IPhone
		#Else If __TARGET__="emscripten"
		Return RuntimePlatform.WebGL
		#Else If __TARGET__="wasm"
		Return RuntimePlatform.WebGL
		#Else If __TARGET__="raspbian"
		Return RuntimePlatform.Raspbian
		#Endif
	End
	
	#Rem monkeydoc The language (two first letters) the user's operating system is running in.
	
	You can use this to automatically pick a localization for your content based on the user's system language.
	
	TODO: !!! not implemented yet !!! and always return "en".
	#End
	Property systemLanguage:String()
		Return "en"
	End
	
	#Rem monkeydoc Create an application. Only one instance is allowed.
	#End
	Method New()
		
		Assert( Application=Null,"Only one instance of MuApp is allowed!" )
		Application=Self
	End
	
	#Rem monkeydoc Initialize application window.
	#End
	Method Init( title:String,width:Int,height:Int,flags:WindowFlags=Null )
		
		New AppInstance
		
		Local wnd:=New CluquaWindow( title,width,height,flags )
		wnd.RenderCallback=Application.Render
		
		wnd.FocusChangedCallback+=Lambda( focused:Bool )
			
			If Not focused
				_lostFocusTime=Millisecs()
			Else
				Local delta:=Millisecs()-_lostFocusTime
				TimingBridge_App.UpdateRealTime( delta )
			Endif
		End
		
	End
	
	#Rem monkeydoc Run application, so all of object now can be updated and rendered.
	
	This method call the Initialized callback.
	After initialized callback there are calls to all Awake() then all Start() of behaviours added inside of Initialized().
	#End
	Method Run()
		
		PoolSystemBridge_App.Init()
		
		Initialized()
		App.Run()
	End
	
	
	Private 
	
	Const INIT_NONE:=0
	Const INIT_LOADING:=1
	Const INIT_DONE:=2
	
	Field _initState:Int
	Field _lostFocusTime:Int
	
	
	Method Render( canvas:Canvas )
		
		If _initState < INIT_DONE
			RenderLoadingCallback( canvas )
			If _initState=INIT_NONE
				_initState=INIT_LOADING
				Start() 'need to make it async, and set _initState=2 when loaded
			Endif
			Return
		Endif
		
		Update()
		
		Local t:=Millisecs()
		
		CameraBridge_App.Render( canvas )
		
		t=Millisecs()-t
		
		TimingBridge_App.UpdateDeltaTime( t )
		
	End
	
	Method Update()
	
		ScriptExecutorBridge_App.ExecuteUpdate()
		ScriptExecutorBridge_App.ExecuteLateUpdate()
		
		GameObjectBridge_App.DestroyObjects() 'destroy after all updates
	End
	
	Method Start()
	
		ScriptExecutorBridge_App.ExecuteAwake()
		ScriptExecutorBridge_App.ExecuteStart()
		_initState=INIT_DONE
	End
	
End



Private

Class CluquaWindow Extends Window
	
	Field RenderCallback:Void( canvas:Canvas )
	Field FocusChangedCallback:Void( focused:Bool )
	
	
	Method New( title:String,width:Int,height:Int,flags:WindowFlags=Null )
	
		Super.New( title,New Recti( 0,0,width,height ),flags|WindowFlags.Center )
		
		DisplayBridge_App.Adjust( Width,Height )
	End
	
	Method OnRender( canvas:Canvas ) Override
		
		RenderCallback( canvas )
		App.RequestRender()
	End
	
	Method OnWindowEvent( event:WindowEvent ) Override
	
		Select event.Type
			Case EventType.WindowMoved
			Case EventType.WindowResized
				App.RequestRender()
			
			Case EventType.WindowLostFocus
				FocusChangedCallback( False )
				
			Case EventType.WindowGainedFocus
				FocusChangedCallback( True )
				
			Default
				Super.OnWindowEvent( event )
		End
	End
	

End


Class CameraBridge_App Extends Camera Abstract

	Function Render( canvas:Canvas )
	
		Camera.RenderAll( canvas )
	End
	
End


Class GameObjectBridge_App Extends GameObject Abstract

	Function DestroyObjects()
	
		GameObject.DestroyObjects()
	End
	
	Private
	
	Method New( name:String )
		Super.New( name )
	End
	
End


Class ScriptExecutorBridge_App Extends ScriptExecutor Abstract
	
	Function ExecuteAwake:Void()
		ScriptExecutor.ExecuteAwake()
	End
		
	Function ExecuteStart:Void()
		ScriptExecutor.ExecuteStart()
	End
	
	Function ExecuteUpdate:Void()
		ScriptExecutor.ExecuteUpdate()
	End
	
	Function ExecuteLateUpdate:Void()
		ScriptExecutor.ExecuteLateUpdate()
	End
	
End


Class DisplayBridge_App Extends DisplayClass Abstract

	Function Adjust( w:int,h:Int )
	
		Display.Adjust( w,h )
	End
	
End


Class TimingBridge_App Extends TimingClass Abstract

	Function UpdateDeltaTime( value:Float )
	
		Timing.UpdateDeltaTime( value )
	End
	
	Function UpdateRealTime( delta:Float )
	
		Timing.UpdateRealTime( delta )
	End
End


Class PoolSystemBridge_App Extends PoolSystem Abstract

	Function Init()
		
		PoolSystem.Init()
	End
	
End
