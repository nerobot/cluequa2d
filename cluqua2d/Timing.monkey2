
Namespace cluqua2d


#Rem monkeydoc Global instance of TimingClass.
@see TimingClass
#End
Global Timing:=New TimingClass


#Rem monkeydoc The interface to get time information.
#End
Class TimingClass

	#Rem monkeydoc The time in seconds it took to complete the last frame (Read Only).
	Use this function to make your game frame rate independent.
	
	If you add or subtract to a value every frame chances are you should multiply with Timing.deltaTime.
	When you multiply with Timing.deltaTime you essentially express: I want to move this object 10 meters per second instead of 10 meters per frame.
	#End
	Property deltaTime:Float()
		Return _deltaTime
	End
	
	#Rem monkeydoc Always return 0.02. Not implemented to framework yet.
	#End
	Property fixedDeltaTime:Float()
		Return _fixedDeltaTime
	End
	
	#Rem monkeydoc The scale at which the time is passing. This can be used for slow motion effects.
	When timeScale is 1.0 the time is passing as fast as realtime. When timeScale is 0.5 the time is passing 2x slower than realtime.
	
	When timeScale is set to zero the game is basically paused if all your functions are frame rate independent.
	
	#End
	Property timeScale:Float()
		Return _timeScale
	Setter( value:Float )
		_timeScale=value
	End
	
	#Rem monkeydoc The real time in seconds since the game started (Read Only).
	In almost all cases you can and should use Timing.time instead.
	
	Returns the time since startup, not affected by Timing.timeScale.
	Also it keeps increasing while the player is paused (in the background).
	Using realtimeSinceStartup is useful when you want to pause the game by setting Timing.timeScale to zero, but still want to be able to measure time somehow.
	#End
	Property realtimeSinceStartup:Float()
		Return _realTime
	End
	
	#Rem monkeydoc The time at the beginning of this frame (Read Only).
	This is the time in seconds since the start of the game.
	
	Returns the same value if called multiple times in a single frame.
	
	Affected with timeScale.
	#End
	Property time:Float()
		Return _time
	End
	
	#Rem monkeydoc The total number of frames that have passed (Read Only).
	#End
	Property frameCount:Int()
		Return _frameCount
	End
	
	
	Protected
		
	Method UpdateDeltaTime( deltaMs:Float )
		deltaMs/=1000.0 'convert into secs
		If deltaMs < 0.0166 Then deltaMs=0.0166
		_realTime+=deltaMs
		_deltaTime=deltaMs*_timeScale
		_time+=_deltaTime
		_frameCount+=1
	End
	
	Method UpdateRealTime( deltaMs:Float )
		_realTime+=deltaMs
		Print "real ms: "+deltaMs
	End
		
	
	Private 
	
	Field _deltaTime:=0.0166
	Field _fixedDeltaTime:=0.02
	Field _timeScale:=1.0
	Field _time:=1.0
	Field _realTime:=1.0
	Field _frameCount:=0
		
	#rem
	Global fixedTime:Float
	Global maximumDeltaTime:Float
	Global renderedFrameCount:Int
	Global smoothDeltaTime:Float
	Global timeSinceLevelLoad:Float
	Global unscaledDeltaTime:Float
	Global unscaledTime:Float
	#end
	
End
