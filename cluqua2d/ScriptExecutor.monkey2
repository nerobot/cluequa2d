
Namespace cluqua2d


#Rem monkeydoc CLass for internal purposes. Should be hidden.
#End
Class ScriptExecutor

	Protected
	
	Function GrabComponent( comp:Component )
		
		Local beh:=Cast<Behaviour>( comp )
		If Not beh Return
		
		Local awk:=Cast<IBehAwake>( beh )
		If awk Then _listForAwake.Push( New BehWrapper<IBehAwake>( awk,beh ) )
		
		Local strt:=Cast<IBehStart>( beh )
		If strt Then _listForStart.Push( New BehWrapper<IBehStart>( strt,beh ) )
		
		Local upd:=Cast<IBehUpdate>( beh )
		If upd Then _listForUpdate.Push( New BehWrapper<IBehUpdate>( upd,beh ) )
		
		Local updLate:=Cast<IBehLateUpdate>( beh )
		If updLate Then _listForLateUpdate.Push( New BehWrapper<IBehLateUpdate>( updLate,beh ) )
		
		Local rend:=Cast<IBehRender>( beh )
		If rend Then _listForRender.Push( New BehWrapper<IBehRender>( rend,beh ) )
		
	End
	
	Function DeleteComponent( comp:Component )
		
		Local beh:=Cast<Behaviour>( comp )
		If Not beh Return
				
		Local awk:=Cast<IBehAwake>( beh )
		If awk
			RemoveFromStack( _listForAwake,awk,BehWrapper<IBehAwake>.Equals )
		Endif
		
		Local strt:=Cast<IBehStart>( beh )
		If strt
			RemoveFromStack( _listForStart,strt,BehWrapper<IBehStart>.Equals )
		Endif
		
		Local upd:=Cast<IBehUpdate>( beh )
		If upd
			RemoveFromStack( _listForUpdate,upd,BehWrapper<IBehUpdate>.Equals )
		Endif
		
		Local updLate:=Cast<IBehLateUpdate>( beh )
		If updLate
			RemoveFromStack( _listForLateUpdate,updLate,BehWrapper<IBehLateUpdate>.Equals )
		Endif
		
		Local rend:=Cast<IBehRender>( beh )
		If rend
			RemoveFromStack( _listForRender,rend,BehWrapper<IBehRender>.Equals )
		Endif
		
	End
	
	Function ExecuteAwake:Void()
		For Local i:=EachIn _listForAwake
			If i.state.isActiveAndEnabled Then i.source.Awake()
		Next
	End
	
	Function ExecuteStart:Void()
		For Local i:=EachIn _listForStart
			If i.state.isActiveAndEnabled Then i.source.Start()
		Next
	End
	
	Function ExecuteUpdate:Void()
		For Local i:=EachIn _listForUpdate
			If i.state.isActiveAndEnabled Then i.source.Update()
		Next
	End
	
	Function ExecuteLateUpdate:Void()
		For Local i:=EachIn _listForLateUpdate
			If i.state.isActiveAndEnabled Then i.source.LateUpdate()
		Next
	End
	
	Function ExecuteRender( canvas:Canvas )
		For Local i:=EachIn _listForRender
			If i.state.isActiveAndEnabled Then i.source.Render( canvas )
		Next
	End
	
	
	Private
	
	Global _listForAwake:=New Stack<BehWrapper<IBehAwake>>
	Global _listForStart:=New Stack<BehWrapper<IBehStart>>
	Global _listForUpdate:=New Stack<BehWrapper<IBehUpdate>>
	Global _listForLateUpdate:=New Stack<BehWrapper<IBehLateUpdate>>
	Global _listForRender:=New Stack<BehWrapper<IBehRender>>

End


Private

' how to make it better?
' what we can replace this wrapper with?

Class BehWrapper<T>
	
	Field state:IEnabled
	Field source:T
	
	Method New ( source:T,state:IEnabled )
	
		Self.source=source
		Self.state=state
	End
	
	Function Equals:Bool( lhs:BehWrapper<T>,rhs:T )
		Return lhs.source=rhs
	End
	
End
