
Namespace cluqua2d


#Rem monkeydoc Global instance for using Display class.
#End
Global Display:=New DisplayClass


#Rem monkeydoc Provides access to a display / screen for rendering operations.
#End
Class DisplayClass

	#Rem monkeydoc Horizontal native display resolution.
	#End
	Property systemWidth:Int()
		Return _sysWidth
	End
	
	#Rem monkeydoc Vertical native display resolution.
	#End
	Property systemHeight:Int()
		Return _sysHeight
	End
	
	#Rem monkeydoc Half of horizontal native display resolution.
	#End
	Property halfSystemWidth:Int()
		Return _sysHalfWidth
	End
	
	#Rem monkeydoc Half of vertical native display resolution.
	#End
	Property halfSystemHeight:Int()
		Return _sysHalfHeight
	End
	
	#Rem monkeydoc Vertical resolution that the display is rendering at.
	#End
	Property renderingHeight:Int()
		Return _renderHeight
	End
	
	#Rem monkeydoc Horizontal resolution that the display is rendering at.
	#End
	Property renderingWidth:Int()
		Return _renderWidth
	End
	
	#Rem monkeydoc Sets rendering resolution for the display.
	
	Rendering resolution can be set up to be different from "native" display resolution.
	#End
	Method SetRenderingResolution( w:Int,h:Int )
		
		_renderWidth=w
		_renderHeight=h
	End
	
	Protected
	
	Method Adjust( w:int,h:Int )
		
		_sysWidth=w
		_sysHeight=h
		
		_sysHalfWidth=w*.5
		_sysHalfHeight=h*.5
		
		If _renderWidth < 1 'if not set
			_renderWidth=w
			_renderHeight=h
		Endif
	End
	
	Field _sysWidth:Int,_sysHalfWidth:Int
	Field _sysHeight:Int,_sysHalfHeight:Int
	Field _renderWidth:Int,_renderHeight:Int
	
End
