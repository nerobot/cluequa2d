
Namespace cluqua2d


#Rem monkeydoc Base class for everything attached to GameObjects.

Note that your code will never directly create a Component. Instead, you write script code, and attach the script to a GameObject.
#End
Class Component

	#Rem monkeydoc The name of game object.
	#End
	Property name:String()
		Return gameObject.name
	Setter( value:String )
		gameObject.name=value
	End
	
	#Rem monkeydoc The tag of game object.
	#End
	Property tag:String()
		Return _gameObject.tag
	Setter( value:String )
		gameObject.tag=value
	End
	
	#Rem monkeydoc The Transform attached to this game object.
	#End
	Property transform:Transform()
		Return _gameObject.transform
	End
	
	#Rem monkeydoc The game object this component is attached to. A component is always attached to a game object.
	#End
	Property gameObject:GameObject()
		Return _gameObject
	End
	
	#Rem monkeydoc Is only one instance of this component can be attached to game object.
	Override this method to make component unique. Default is false.
	#End
	Property isUnique:Bool() Virtual
		Return False
	End
	
	
	Protected
	
	Field _gameObject:GameObject
	
	#Rem monkeydoc Callback to catch component destruction moment.
	#End
	Method OnDestroy() Virtual
	End
	
	#Rem monkeydoc Callback to catch component attachment to game object moment.
	It's useful to be subscribed to in constructor of component when we have no info about game object.
	#End
	Method OnAttached() Virtual
	End
	
End
