
Namespace cluqua2d


#Rem monkeydoc Represents a Sprite object for use in 2D gameplay.
Sprites are 2D graphic objects used for characters, props, projectiles and other elments of 2D gameplay.
The graphics are obtained from bitmap images - Image.

The Sprite class primarily identifies the section of the image that should be used for a specific sprite.
This information can then be used by a SpriteRenderer component on a GameObject to actually display the graphic.

See Also: SpriteRenderer class.
#End
Class Sprite
	
	#Rem monkeydoc Image that actually rendered.
	#End
	Property image:Image()
		Return _image
	End
	
	#Rem monkeydoc Get the reference to the used image (atlas or single image).
	#End
	Property atlas:Image()
		Return _atlas
	Setter( value:Image )
		_atlas=value
		AdjustAtlas()
	End
	
	#Rem monkeydoc Return width of this sprite.
	#End
	Property width:Float()
		Return _image ? _image.Width Else 0
	End
	
	#Rem monkeydoc Return height of this sprite.
	#End
	Property height:Float()
		Return _image ? _image.Height Else 0
	End
	
	#Rem monkeydoc Return size of sprite in pixels.
	#End
	Property size:Vec2f()
	
		Return New Vec2f( _region.Width,_region.Height )
	End
	
	#Rem monkeydoc Location of the Sprite's center point in the Rect on the original Image, specified in pixels.
	#End
	Property pivot:Vec2f()
		Return _pivot
	Setter( value:Vec2f )
		_pivot=value
	End
	
	#Rem monkeydoc Location of the Sprite on the original Image (atlas or single image), specified in pixels.
	#End
	Property region:Recti()
		Return _region
	End
	
	
	Protected
	
	#Rem monkeydoc Creates new sprite object from image source.
	In custom case atlas is a single image.
	If region is null, (0,0,atlas.Width,atlas.Height) will be used.
	If anchor is null, Anchor.HCenterVCenter will be used.
	#End
	Method New( name:String,atlas:Image,region:Recti=Null,anchor:Vec2f=Null )
		
		_name=name
		If region=Null And atlas<>Null
			_region=New Recti( 0,0,atlas.Width,atlas.Height )
		Else
			_region=region
		Endif
		Self.atlas=atlas
		If anchor=Null Then anchor=Anchor.HCenterVCenter
		_pivot=New Vec2f( width,height )*anchor
	End
	
	
	Private
	
	Field _atlas:Image
	Field _image:Image
	Field _pivot:Vec2f
	Field _region:Recti
	Field _name:String
	
	Method AdjustAtlas()
		_image=Null
		If _atlas=Null Return
		
		_image=New Image( _atlas,_region )
		If _pivot=Null
			_pivot=New Vec2f( width*.5,height*.5 )
		Endif
	End
	
End

#Rem monkeydoc Anchor values to use for align.
+-----+-----+
|    |   |
+-----+-----+
|    |   |
+-----+-----+
#End
Struct Anchor

	Const LeftTop:=New Vec2f
	Const LeftVCenter:=New Vec2f ( 0,.5 )
	Const LeftBottom:=New Vec2f ( 0,1 )
	Const HCenterTop:=New Vec2f ( .5,0 )
	Const HCenterVCenter:=New Vec2f ( .5,.5 )
	Const HCenterBottom:=New Vec2f ( .5,1 )
	Const RightTop:=New Vec2f ( 1,0 )
	Const RightVCenter:=New Vec2f ( 1,.5 )
	Const RightBottom:=New Vec2f ( 1,1 )
	
End
