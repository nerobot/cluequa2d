Namespace cluqua2d


#Rem monkeydoc PoolSystem class helps to manage objects that should be pooled.
#End
Class PoolSystem

	#Rem monkeydoc Get available game object from pool.
	#End
	Function PopGameObject:GameObject()
		Return _gameObjects.Acquire()
	End
	
	#Rem monkeydoc Return back unused game object into pool.
	#End
	Function PushGameObject( go:GameObject )
		_gameObjects.Release( go )
	End
	
	
	Protected
	
	Function Init()
	
		_gameObjects=New ObjectPool<GameObject>( 50,10,CreateNewGameObject )
	End
	
	
	Private
	
	Global _gameObjects:ObjectPool<GameObject>
	
	Function CreateNewGameObject:GameObject()
		Return New GameObject( "" )
	End
	
End


' Base of ObjectPool class was taken from pyro.framework


#Rem monkeydoc The ObjectPool class.
#End
Class ObjectPool<T>

	#Rem monkeydoc Create new instance of object pool.
	@param capacity Initial capacity for this pool.
	@param capacityIncrement step to increase pool size.
	@param createNewFunc function that will be used to create new objects (not null).
	@param resetFunc function that will be used to reset objects before acquire (can be null).
	#End
	Method New( capacity:Int,capacityIncrement:Int,createNewFunc:T(),resetFunc:Void(T)=Null )
		
		_data=New T[capacity]
		_inUse=New Bool[capacity]
		
		_createNewFunc=createNewFunc
		_resetFunc=resetFunc
		_capacityIncrement=capacityIncrement
		
		For Local i:=0 Until capacity
			Add( createNewFunc() )
		Next
	End
	
	#Rem monkeydoc Adds an oject to the pool.
	#End
	Method Add( obj:T )

		If _size=_data.Length
			_data=ResizeArray( _data,_size+_capacityIncrement )
			_inUse=ResizeArray( _inUse,_size+_capacityIncrement )
		Endif

		_data[_size]=obj

		_size+=1
	End

	#Rem monkeydoc Empties the pool.
	#End
	Method Clear()
		For Local i:=0 Until _size
			_data[i]=Null
			_inUse[i]=False
		Next
		_size=0
	End

	#Rem monkeydoc Return the number of objects in the pool.
	#End
	Property Size:Int()
		Return _size
	End

	#Rem monkeydoc Changes the state of the object to 'available'.
	#End
	Method Release:Bool( obj:T )
		For Local i:=0 Until _size
			If _data[i]=obj
				_inUse[i]=False
				Return True
			Endif
		Next
		Return False
	End

	#Rem monkeydoc Changes the state of the object at given index to 'available'.
	#End
	Method Release( index:Int )
		_inUse[index]=False
	End

	#Rem monkeydoc Changes the state of all the objects to 'available'.
	#End
	Method ReleaseAll()
		For Local i:=0 Until _size
			_inUse[i]=False
		Next
	End

	#Rem monkeydoc Returns the first available object in the pool.
	
	If there is no available object and flag autoIncrease is set to True (by default) then new object will be created, inserted to the pool and returned.
	#End
	Method Acquire:T( reset:Bool=True )

		Local count:=_size

		While count>0

			Local i:=_index

			If Not _inUse[i]

				_inUse[i]=True

				_index+=1
				If _index>_size-1 _index=0

				Local result:=_data[i]
				If reset And _resetFunc<>Null Then _resetFunc( result )
				Return result

			Endif

			_index+=1
			If _index>_size-1 _index=0

			count-=1

		Wend

		If _capacityIncrement > 0
			Local obj:=_createNewFunc()
			Add( obj )
			Return obj
		Endif
		
		Return Null
		
	End
	
	#Rem monkeydoc Returns the object at the specified index.
	#End
	Operator []:T( index:Int )
		Return _data[index]
	End
	
	Private

	Field _data:T[]
	Field _index:=0
	Field _inUse:Bool[]
	Field _size:=0
	Field _createNewFunc:T()
	Field _resetFunc:Void(T)
	Field _capacityIncrement:Int
	
End
