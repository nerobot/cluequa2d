
Namespace cluqua2d


#Rem monkeydoc Renders a Sprite.
#End
Class SpriteRenderer Extends Renderer

	#Rem monkeydoc The Sprite to render.
	#End
	Property sprite:Sprite()
		Return _sprite
	Setter( value:Sprite )
		_sprite=value
	End
	
	#Rem monkeydoc Flips the sprite on the X axis.
	
	Only the rendering is affected. Use negative Transform.scale, if you want to affect all the other components (for example colliders).
	
	TODO: !!! not implemented yet !!!
	#End
	Field flipX:=False
	
	#Rem monkeydoc Flips the sprite on the Y axis.
	
	Only the rendering is affected. Use negative Transform.scale, if you want to affect all the other components (for example colliders).
	
	TODO: !!! not implemented yet !!!
	#End
	Field flipY:=False
	
	#Rem monkeydoc Size if sprite if there is any or (0,0).
	#End
	Property size:Vec2i()
		
		Local w:=0.0,h:=0.0
		If _sprite<>Null
			w=_sprite.width
			h=_sprite.height
		Endif
		Return New Vec2i( w,h )
	End
	
	' allow only one sprite renderer per game object.
	Property isUnique:Bool() Override
		Return True
	End
	
	
	Protected
	
	Method Render( canvas:Canvas ) Override
		
		If Not( _sprite Or _sprite.image) Return
		
		Local clr:=canvas.Color
		Local a:=canvas.Alpha
		
		canvas.Color=color
		canvas.Alpha=alpha
		canvas.PushMatrix()
		
		canvas.Matrix=canvas.Matrix*transform.matrix
		
		Local piv:=_sprite.pivot
		canvas.DrawImage( _sprite.image,-piv.x,-piv.y )
		
		canvas.PopMatrix()
		canvas.Color=clr
		canvas.Alpha=a
	End
	
	
	Private

	Field _sprite:Sprite
		
End
