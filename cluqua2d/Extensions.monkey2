
Namespace cluqua2d


#Rem monkeydoc Converts radians to degrees.
#End
Function ToDegrees:Float( rad:Float )
	Return rad * Pi_div_180_inverse
End

#Rem monkeydoc Converts degrees to radians.
#End
Function ToRadians:Float( degree:Float )
	Return degree * Pi_div_180
End

#Rem monkeydoc Remove value from stack by using 'equals' method.
Note that value has a different type that containing in stack.

With this function we not need to overload '=' for all-types-with-others.
Using equals-method right here as param.
#End
Function RemoveFromStack<T,V>:Bool( stack:Stack<T>,value:V,equals:Bool( lhs:T,rhs:V ) )
	
	For Local i:=0 Until stack.Length
		If equals( stack[i],value )
			stack.Erase( i )
			Return True
		Endif
	Next
	Return False
End

#Rem monkeydoc Extract rotation component from AffineMat3f.
#End
Function AffineGetRotation:Float( mat:AffineMat3f )
	
	Local i:=mat.i
	Return ATan2( i.y,i.x )
End

#Rem monkeydoc Extract scale component from AffineMat3f.
#End
Function AffineGetScale:Vec2f( mat:AffineMat3f )
	
	Local i:=mat.i,j:=mat.j
	Local sx:=Sqrt( i.x*i.x + j.x*j.x )
	Local sy:=Sqrt( i.y*i.y + j.y*j.y )
	Return New Vec2f( sx,sy )
End

#Rem monkeydoc Extract position component from AffineMat3f.
#End
Function AffineGetPosition:Vec2f( mat:AffineMat3f )

	Return mat.t
End

#Rem monkeydoc Resize array to given len.
#End
Function ResizeArray<T>:T[]( arr:T[],len:Int )
	
	If arr.Length=len Return arr
	Local out:=New T[len]
	arr.CopyTo( out,0,0,Min( len,arr.Length ) )
	Return out
End


Private

Const Pi_div_180:=3.1415926535897931/180.0
Const Pi_div_180_inverse:=180.0/3.1415926535897931
