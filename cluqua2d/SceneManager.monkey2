
Namespace cluqua2d


#Rem monkeydoc Scene manager.

There are may be several scenes in the project.
All scenes is created by this class.
#End
Class SceneManager

	#Rem monkeydoc Create a new scene.
	#End
	Function NewScene:Scene( name:String )
		
		Assert( Not GetScene( name ), "Scene with name "+name+" already exists!" )
		
		Local scene:=SceneBridge_SceneMan.NewScene( name )
		_scenes.Add( scene )
		Return scene
		
	End
	
	#Rem monkeydoc Get scene by name.
	#End
	Function GetScene:Scene( name:String )
		
		For Local s:=Eachin _scenes
			If s.name = name Return s
		Next
		Return Null
	End
	
	
	Protected
	
	Global _scenes:=New Stack<Scene>
			
End



Private 


Class SceneBridge_SceneMan Extends Scene Abstract

	Function NewScene:Scene( name:String )
		Return New Scene( name )
	End
	
	
	Protected
	
	Method New( name:String )
		Super.New( name )
	End
		
End

