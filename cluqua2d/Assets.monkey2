
Namespace cluqua2d


#Rem monkeydoc Assets class manages resources in project.
It is recomended to load all images via Assets (GetImage).

Root filder for all resources is \assets\.
#End
Class Assets
	
	#Rem monkeydoc To simplify 'batched' loading resources from custom folder you can set that folder path and use short sub-paths.
	#End
	Function SetFolder:Void( folder:String )
		
		_folder="asset::"+folder
	End
	
	#Rem monkeydoc Get image with path from memory or load it if not loaded yet.
	#End
	Function GetImage:Image( path:String )
		
		path=_folder+path
		Local img:=_images[path]
		If Not img
			img=Image.Load( path )
			If img
				_images.Add( path,img )
			Else
				Print "bad resource: "+path
			Endif
		Endif
		Return img
		Return Null
	End
	
	#Rem monkeydoc Load string from path.
	#End	
	Function LoadString:String( path:String )
		
		path=_folder+path
		Return LoadString( path )
	End
	

	Private

	Global _images:=New StringMap<Image>
	Global _folder:="asset::"

End
