
Namespace cluqua2d


#Rem monkeydoc Allow to operate with layers. !!! This time used for internal framework purposes. !!!
There are totally 32 layers available.
#End
Class LayerManager
	
	Protected
	
	#Rem monkeydoc Get a layer by index [0..31].
	#End
	Function GetLayer:Layer( index:Int )
		Return _layers[index]
	End
	
	#Rem monkeydoc Place game object to the layer.
	#End
	Function Assign:Void( go:GameObject,layer:Int=0 )
	
		layer=Clamp( layer,0,MaxLayers )
	
		If Not _layers
			Init()
		Endif
	
		Local prev:=go.layer
	
		If _layers[prev]<>Null
			_layers[prev].Remove( go )
		Endif
	
		_layers[layer].Add( go )
	End
	
	
	Private
	
	Const MaxLayers:=32
	Global _layers:Layer[]
	
	Function Init()
	
		_layers=New Layer[MaxLayers]
		For Local i:=0 Until MaxLayers
			_layers[i]=New Layer
		Next
	End
	
End


Private

#Rem monkeydoc The layer class. All object are placed in one of 32 layers.
#End
Struct Layer
	
	#Rem monkeydoc Append game object to this layer.
	#End
	Method Add( go:GameObject )
		_items.Push( go )
	End
	
	#Rem monkeydoc Remove game object from this layer.
	#End
	Method Remove( go:GameObject )
		_items.Remove( go )
	End
	
	
	Private
	
	Field _items:=New Stack<GameObject>
	
End
