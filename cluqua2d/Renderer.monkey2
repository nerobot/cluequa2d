
Namespace cluqua2d


#Rem monkeydoc General functionality for all renderers.
A renderer is what makes an object appear on the screen.

Use this class to access the renderer of any object.
Renderers can be disabled to make objects invisible (see enabled).

You also can use combination of [Extends MonkeyBehaviour Implements IBehRender] instead of [Extends Renderer + AddComponent<MyRenderer>()]
#End
Class Renderer Extends Behaviour Implements IBehRender
	
	#Rem monkeydoc 
	#End
	Field color:=Color.White
	
	#Rem monkeydoc 
	#End
	Property alpha:Float()
		Return _alpha
	Setter( value:Float )
		_alpha=Clamp( value,0.0,1.0 )
	End
	
	
	Protected
	
	Method Render( canvas:Canvas ) Abstract
	
	
	Private
	
	Field _alpha:=1.0
	
End
