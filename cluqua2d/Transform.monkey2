
Namespace cluqua2d


#Rem monkeydoc The coordinate space in which to operate.

|Value|Description
|:------------------|:-----------
|World|Applies transformation relative to the world coordinate system.
|Me|Applies transformation relative to the local coordinate system.
#End
Enum Space
	World,
	Me
End


#Rem monkeydoc Position, rotation and scale of an object.
Every object in a scene has a Transform. It's used to store and manipulate the position, rotation and scale of the object.
Every Transform can have a parent, which allows you to apply position, rotation and scale hierarchically.
#End
Class Transform Extends Component Final
	
	#Rem monkeydoc Resulting matrix for this transform.
	#End
	Property matrix:AffineMat3f()
		Return _mat
	End
	
	#Rem monkeydoc The number of children the Transform has.
	#End
	Property childCount:Int()
		Return _children<>Null ? _children.Length Else 0
	End
	
	#Rem monkeydoc Children iterator.
	#End
	Property children:Stack<Transform>.Iterator()
		Return _children<>Null ? _children.All() Else Null
	End
	
	#Rem monkeydoc The parent of the transform.
	#End
	Property parent:Transform()
		Return _parent
	Setter( value:Transform )
		SetParent( value )
	End
	
	#Rem monkeydoc The position of the transform in world space.
	#End
	Property position:Vec2f()
	
		Return AffineGetPosition( _mat )
	Setter( value:Vec2f )
	
		value=FixCoord( value )
		Local pos:=FixCoord( AffineGetPosition( _mat ) )
		Local delta:=value-pos
		Local t:=_locMat.t
		_locMat.t=t+delta
		_localPosition+=delta/AffineGetScale( _locMat )
		CalcMat()
	End
	
	#Rem monkeydoc Position of the transform relative to the parent transform.
	#End
	Property localPosition:Vec2f()
	
		Return _localPosition		
	Setter( value:Vec2f )
		
		value=FixCoord( value )
		Local delta:=value-_localPosition
		_localPosition=value
		_locMat.t=_locMat.t+delta
		CalcMat()
	End
	
	#Rem monkeydoc The global scale of the object (Read Only).
	#End
	Property scale:Vec2f()
	
		Return AffineGetScale( _mat )
	End
	
	#Rem monkeydoc The scale of the transform relative to the parent.
	#End
	Property localScale:Vec2f()
	
		Return _localScale
	Setter( value:Vec2f )
	
		Local delta:=value/_localScale
		_localScale=value
		_locMat=_locMat.Scale( delta )
		CalcMat()
	End
	
	#Rem monkeydoc The rotation of the transform in world space (in degrees).
	#End
	Property rotation:Float()
	
		Return AffineGetRotation( _mat )
	Setter( value:Float )
	
		value = value Mod 360.0
		Local rot:=AffineGetRotation( _mat )
		Local delta:=ToRadians(value)-rot
		_locMat=_locMat.Rotate( -delta )
		CalcMat()
	End
	
	#Rem monkeydoc The rotation of the transform relative to the parent transform's rotation (in degrees).
	#End
	Property localRotation:Float()
	
		Return _localRotation
	Setter( value:Float )
	
		value = value Mod 360.0
		Local delta:=ToRadians( _localRotation-value )
		_localRotation=value
		_locMat=_locMat.Rotate( delta )
		CalcMat()
	End
	
	#Rem monkeydoc Reset transform to localPosition(0,0), localScale(1,1), localRotation(0).
	#End
	Method Reset()
		
		_localPosition=New Vec2f
		_localScale=New Vec2f( 1,1 )
		_localRotation=0
		_locMat=New AffineMat3f
		CalcMat()
	End
	
	#Rem monkeydoc Returns a transform child by index.
	#End
	Method GetChild:Transform( index:Int )
		Return _children[index]
	End
	
	#Rem monkeydoc Returns a transform child by index in indexer-style: t=transform[i].
	#End
	Operator []:Transform( index:Int )
		Return _children[index]
	End
	
	#Rem monkeydoc Set the parent of the transform.
	#End
	Method SetParent:Void( parent:Transform )
	
		If parent=_parent Return
		If _parent And _parent.childCount>0
			_parent._children.Remove( Self )
		Endif
		_parent=parent
		If _parent
			If _parent._children=Null Then _parent._children=New Stack<Transform>
			_parent._children.Push( Self )
			'Local delta:=_position-parent._position
			'_localPosition=delta
			CalcMat( Self )
		EndIf
	End
	
	#Rem monkeydoc Is this transform a child of parent?
	#End
	Method IsChildOf:Bool( parent:Transform )
	
		Return IsChildOfInternal( Self,parent )
	End
	
	#Rem monkeydoc Rotates the transform so the forward vector points at target's current position.
	#End
	Method LookAt( target:Vec2f )
	
		Assert( False,"Not implemented!" )
	End
	
	#Rem monkeydoc Applies a rotation.
	#End
	Method Rotate( angle:Float )
	
		localRotation=localRotation+angle
	End
	
	#Rem monkeydoc Rotates the transform through point in world coordinates by angle degrees.
	#End
	Method RotateAround( point:Vec2f,angle:Float )
	
		Assert( False,"Not implemented!" )
	End
	
	#Rem monkeydoc Moves the transform in the direction and distance of translation.
	If relativeTo is left out or set to Space.Me the movement is applied relative to the transform's local axes. If relativeTo is Space.World the movement is applied relative to the world coordinate system.
	#End
	Method Translate( delta:Vec2f,relativeTo:Space=Space.World )
	
		If relativeTo = Space.World
			position+=delta
		Else
			localPosition+=delta
		Endif
	End
	
	#Rem monkeydoc 
	#End
	Method Translate( dx:Float,dy:Float,relativeTo:Space=Space.Me )
	
		Local delta:=New Vec2f( dx,dy )
		localPosition+=delta
	End
	
	#Rem monkeydoc Finds a child by name and returns it.
	If no child with name can be found, null is returned. If name contains a '/' character it will traverse the hierarchy like a path name.
	#End
	Method Find:Transform( name:String )
		
		If name.StartsWith( "/" ) Then name=name.Slice( 1 )
		If name.EndsWith( "/" ) Then name=name.Slice( 0,name.Length-1 )
		
		If name.Find( "/" )=-1
			Return FindSimple( Self,name )
		Else
			Local names:=name.Split( "/" )
			Return FindHierarchy( Self,names,0 )
		Endif
	End
	
	' make me unique
	Property isUnique:Bool() Override
		Return True
	End
	
	Private
	
	Field _localPosition:=New Vec2f
	Field _localScale:=New Vec2f( 1,1 )
	Field _localRotation:=0.0
	Field _parent:Transform
	Field _children:Stack<Transform>
	Field _mat:=New AffineMat3f
	Field _locMat:=New AffineMat3f
	
	Const InvertY:=New Vec2f( 1,-1 )
	
	
	Method OnDestroy() Override
		
		SetParent( Null )
	End
	
	Method IsChildOfInternal:Bool( trans:Transform,parent:Transform )
	
		If trans=parent Return True
	
		If parent._children=Null Return False
	
		For Local t:=Eachin parent._children
	
			Local ok:=IsChildOfInternal( t,parent )
			If ok Return True
		Next
	
		Return False
	End
	
	Method FixCoord:Vec2f( pos:Vec2f )
	
		Return pos'*InvertY
	End
	
	Method CalcMat( updateChildren:Bool=True )
	
		_mat=parent ? parent.matrix*_locMat Else _locMat
	
		If updateChildren Then CalcMatChildren( Self )
	End
	
	Method CalcMatChildren( trans:Transform )
	
		If trans.childCount=0 Return
		
		For Local t:=Eachin trans._children
			t._mat=trans._mat*t._locMat
			CalcMatChildren( t )
		Next
	End
		
	Method FindSimple:Transform( trans:Transform,name:String )
	
		If trans._children<>Null
			For Local t:=Eachin trans._children
				If t.name=name Return t
				Local res:=FindSimple( t,name )
				If res Return res
			Next
		Endif
		Return Null
	End
	
	Method FindHierarchy:Transform( trans:Transform,names:String[],index:Int )
	
		If trans._children<>Null
			Local name:=names[index]
			For Local t:=Eachin trans._children
				If t.name=name
					index+=1
					If index=names.Length Return t
	
					Return FindHierarchy( t,names,index )
				Endif
			Next
		Endif
		Return Null
	End
	
End
