
Namespace cluqua2d


#Rem monkeydoc Scenes contains all game objects.
#End
Class Scene

	#Rem monkeydoc Returns the name of the scene.
	#End
	Property name:String()
		Return _name
	End
	
	#Rem monkeydoc Root transform of the scene. Need to remove?
	#End
	Property root:Transform()
		Return _root
	End
	
	#Rem monkeydoc Enable/Disable this scene and all objects in it.
	#End
	Property enabled:Bool()
		Return root.gameObject.activeSelf
	Setter( value:Bool )
		root.gameObject.SetActive( value )
	End
	
	#Rem monkeydoc Add game object to the scene.
	#End
	Method AddChild( child:GameObject )
		
		If child.scene=Self Return
		
		child.transform.parent=_root
		GameObjectBridge_Scene.SetScene( child,Self )
		
		'TODO: refresh activeInHierarchy state
		'root.gameObject.SetActive( enabled )
	End
	
	
	Protected
	
	Method New( name:String )
		_name=name
		Local go:=New GameObject("SceneRoot")
		_root=go.transform
	End
  
  
	Private
	
	Field _root:Transform
	Field _name:String
	
End


Private

Class GameObjectBridge_Scene Extends GameObject Abstract

	Method New( name:String )
		Super.New( name )
	End
	
	Function SetScene( go:GameObject,scene:Scene )
		go._scene=scene
	End
	
End
