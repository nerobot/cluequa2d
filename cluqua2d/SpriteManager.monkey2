
Namespace cluqua2d


#Rem monkeydoc SpriteManager allow to create sprites from source image (atlas or single image).
All sprites created by this class.
The same sprite can be assigned to many sprite renderers.
#End
Class SpriteManager Final
	
	#Rem monkeydoc Create new sprite
	@param name Name of sprite (unique is the best)
	@param atlasName Name of atlas from which we want to create sprite
	@param region Rect inside of atlas that contains this sprite. If region is null, (0,0,atlas.Width,atlas.Height) will be used.
	@param anchor Anchor to set sprite's pivot. If anchor is null, Anchor.HCenterVCenter will be used.
	#End
	Function CreateSprite:Sprite( name:String,atlasName:String,region:Recti=Null,anchor:Vec2f=Null )
		
		Local atlas:=Assets.GetImage( atlasName )
		Local s:=SpriteBridge.CreateSprite( name,atlas,region,anchor )
		_sprites[name]=s
		Return s
	End
	
	#Rem monkeydoc Get sprite by name. Return null if such name not found.
	#End
	Function GetSprite:Sprite( name:String )
	
		Return _sprites[name]
	End
	
	
	Private
	
	Global _sprites:=New StringMap<Sprite>
	
End


#Rem monkeydoc Create new game object with attached SpriteRenderer component, and assigned sprite to renderer.
@param name Name of created game object
@param spriteName Name of sprite to find by SpriteManager.GetSprite()
#End
Function CreateSprite:GameObject( name:String,spriteName:String )
	
	Local go:=New GameObject( name )
	Local rend:=go.AddComponent<SpriteRenderer>()
	rend.sprite=SpriteManager.GetSprite( spriteName )
	Return go
	
End

#Rem monkeydoc Create new game object with attached SpriteRenderer component, and assigned sprite to renderer.
@param name Name of created game object
@param sprite Reference to a sprite object, that will be assigned to renderer
#End
Function CreateSprite:GameObject( name:String,sprite:Sprite )
	
	Local go:=New GameObject( name )
	Local rend:=go.AddComponent<SpriteRenderer>()
	rend.sprite=sprite
	Return go
	
End


Private

Class SpriteBridge Extends Sprite Abstract

	Function CreateSprite:Sprite( name:String,atlas:Image,region:Recti=Null,anchor:Vec2f=Null )
	
		Return New Sprite( name,atlas,region,anchor )
	End
	
	
	Private
	
	Method New( name:String,atlas:Image,region:Recti=Null,anchor:Vec2f=Null )
		Super.New( name,atlas,region,anchor )
	End
End
