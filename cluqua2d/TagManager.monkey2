
Namespace cluqua2d


#Rem monkeydoc Tag manager class.
Tags must be registered before using them.
#End
Class TagManager
	
	#Rem monkeydoc Add new tag.
	#End
	Function RegisterTag( tag:String )
	
		Add( tag )
	End
	
	#Rem monkeydoc Add new tags.
	#End
	Function RegisterTags( tags:String[] )
	
		For Local i:=Eachin tags
			Add( i )
		Next
	End
	
	
	Protected
		
	Function AddObject( go:GameObject )
	
		Local tag:=go.tag
		If Not tag Return
		
		Assert( _tags.Contains( tag ),"Unregistered tag '"+tag+"'!" )
		
		Local items:=_objects[tag]
		If items=Null
			items=New Stack<GameObject>
			_objects.Add( tag,items )
		Endif
		
		items.Add( go )
	End
	
	Function RemoveObject( go:GameObject )
	
		Local tag:=go.tag
		If Not tag Return
	
		Assert( _tags.Contains( tag ),"Unregistered tag '"+tag+"'!" )
	
		Local items:=_objects[tag]
		If items=Null Return
			
		items.Remove( go )
	End
	
	Function FindObject:GameObject( tag:String,activeOnly:Bool=True )
	
		Assert( tag And _tags.Contains( tag ),"Unregistered tag '"+tag+"'!" )
	
		Local items:=_objects[tag]
		If items=Null Or items.Empty Return Null
		
		If activeOnly
			For Local i:=0 Until items.Length
				if items[0].activeSelf Return items[0]
			Next
			Return Null
		Endif
		Return items[0]
	End
	
	Function FindAllObjects:GameObject[]( tag:String,activeOnly:Bool=True )
	
		Assert( tag And _tags.Contains( tag ),"Unregistered tag '"+tag+"'!" )
	
		Local items:=_objects[tag]
		If items=Null Or items.Empty Return EmptyArray
		
		If activeOnly
			Local list:=New Stack<GameObject>
			For Local i:=Eachin items
				If i.activeSelf Then list.Add( i )
			Next
			Return list.ToArray()
		Else
			Return items.ToArray()
		Endif
	End
	
	
	Private
	
	Const EmptyArray:=New GameObject[0]
	Global _tags:=New Stack<String>
	Global _objects:=New StringMap<Stack<GameObject>>
	
	Function Add( tag:String )
		
		If _tags.Contains( tag ) Return
		_tags.Add( tag )
	End
	
End
