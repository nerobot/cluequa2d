
Namespace cluqua2d

#Rem monkeydoc Special type of game object.

The main advantage of prefabs is: when you change (and apply) any parameter of prefab - this leads to the update all of game objects based on this prefab.

Prefabs can be loaded from file.
#End
Class Prefab Extends GameObject
	
	#Rem monkeydoc Create new prefab from game object.
	#End
	Method New( source:GameObject,name:String=Null )
		
		Super.New( "" )
		_source=source
		If Not name Then name=source.name
		Self.name=name
	End
	
	#Rem monkeydoc Apply changes will refresh all derived game objects according with this prefab.
	TODO: !!! not implemented yet !!!
	#End
	Method ApplyChanges()
	
		Local list:=_objects[Self]
		If list=Null Or list.Empty Return
		
		For Local i:=Eachin list
		
			' need to copy all values from prefab to each game object
			' need full reflection support
		End
	End
		
	#Rem monkeydoc Get prefab by name. Load prefab if not loaded yet.
	TODO: !!! not implemented yet !!!
	#End
	Function GetPrefab:Prefab( name:String )
		
		Return Null
	End
	
	#Rem monkeydoc Create new game object from prefab.
	#End
	Function Instantiate:GameObject( prefabName:String,pos:Vec2f=New Vec2f,scale:Vec2f=New Vec2f(1,1),rotation:Float=0)
		
		Local prefab:=GetPrefab( prefabName )
		Return Instantiate( prefab,pos,scale,rotation )
	End
	
	#Rem monkeydoc Create new game object from prefab.
	#End
	Function Instantiate:GameObject( prefab:Prefab,pos:Vec2f=New Vec2f,scale:Vec2f=New Vec2f(1,1),rotation:Float=0)
		
		Local go:=GameObject.Create( prefab.name )
		Local t:=go.transform
		t.localPosition=pos
		t.localScale=scale
		t.localRotation=rotation
		
		Local list:=_objects[prefab]
		If list=Null
			list=New Stack<GameObject>
			_objects[prefab]=list
		Endif
		
		go.OnDestroy+=Lambda()
			list.Remove( go )
		End
		
		Return go
	End
	
	
	Private
	
	Field _source:GameObject
	Global _objects:Map<Prefab,Stack<GameObject>>
	
	Method New( name:String )
		Super.New( name )
	End
	
End
