
Namespace cluqua2d


#Rem monkeydoc Behaviours are Components that can be enabled or disabled.
See Also: MonkeyBehaviour and Component.
#End
Class Behaviour Extends Component Implements IEnabled
	
	#Rem monkeydoc Enabled Behaviours are Updated, disabled Behaviours are not.
	#End
	Property enabled:Bool()
		Return _enabled
	Setter( value:Bool )
		_enabled=value
	End
	
	#Rem monkeydoc Has the Behaviour had enabled called.
	#End
	Property isActiveAndEnabled:Bool()
		Return _enabled And gameObject.activeInHierarchy
	End
	
	
	Private
	
	Field _enabled:=True
	
End


#Rem monkeydoc MonkeyBehaviour is the base class from which every cluqua2d script derives.
You must explicitly derive from MonkeyBehaviour.

#End
Class MonkeyBehaviour Extends Behaviour
	
	#Rem monkeydoc Execution order for a Behaviour.
	#End
	Property execOrder:Int()
		Return _execOrder
	End
	
	
	Private
	
	Field _execOrder:Int
	
End

#Rem monkeydoc Interface for behaviours to check them enabled state.
#End
Interface IEnabled
	
	Property enabled:Bool()
	Setter( value:Bool )
	
	Property isActiveAndEnabled:Bool()
	
End

#Rem monkeydoc Interface for behaviours to have Awake() callback.
#End
Interface IBehAwake
	Method Awake()
End

#Rem monkeydoc Interface for behaviours to have Start() callback. It called after all Awake()s.
#End
Interface IBehStart
	Method Start()
End

#Rem monkeydoc Interface for behaviours to have Update() callback. It called every frame before Render all.
#End
Interface IBehUpdate
	Method Update()
End

#Rem monkeydoc Interface for behaviours to have LateUpdate() callback. It called every frame after all Update()s.
#End
Interface IBehLateUpdate
	Method LateUpdate()
End

#Rem monkeydoc Interface for behaviours to have Render() callback. It called every frame after all Update()s and LateUpdate()s.
#End
Interface IBehRender
	Method Render( canvas:Canvas )
End
