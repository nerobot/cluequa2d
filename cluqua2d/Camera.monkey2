
Namespace cluqua2d


#Rem monkeydoc Create and return new GameObject with name 'name' and attach Camera component to it.
@param name name of game object
#End
Function CreateCamera:GameObject( name:String )

	Local go:=New GameObject( name )
	go.AddComponent<Camera>()
	Return go
End

#Rem monkeydoc A Camera is a device through which the player views the world.

A screen space point is defined in pixels.
The bottom-left of the screen is (0,0); the right-top is (pixelWidth,pixelHeight).

A viewport space point is normalized and relative to the Camera. The top-left of the Camera is (0,0); the bottom-right is (1,1).
#End
Class Camera Extends Behaviour

	#Rem monkeydoc The first created camera.
	#End
	Global main:Camera
	
	#Rem monkeydoc The color with which the screen will be cleared.
	Default is Color.DarkGrey
	#End
	Field backgroundColor:=Color.DarkGrey
	
	#Rem monkeydoc The aspect ratio (width divided by height).
	#End
	Property aspect:Float()
		Return _aspect
	End
	
	#Rem monkeydoc Viewport is the area in percents of screen size in range [0..1]
	Default is [0,0,1,1] - whole screen.
	#End
	Property viewport:Rectf()
	
		Return _viewport
	Setter( value:Rectf )
		
		Local prev:=_viewportPixels.Size
		
		_viewport=value
		_viewportPixels=_viewport*_size
		
		Local delta:=_viewportPixels.Size-prev
		_mat=_mat.Translate( delta*.5 )
	End
	
	#Rem monkeydoc How wide is the camera in pixels (Read Only).
	#End
	Property pixelWidth:Int()
		Return _size.x
	End
	
	#Rem monkeydoc How tall is the camera in pixels (Read Only).
	#End
	Property pixelHeight:Int()
		Return _size.y
	End
	
	#Rem monkeydoc How wide/tall is the camera in pixels (Read Only).
	#End
	Property size:Vec2f()
		Return _size
	End
	
	#Rem monkeydoc Zoom of camera view.
	#End
	Property zoom:Vec2f()
		Return _zoom
	Setter( value:Vec2f )
		
		Local delta:=value/_zoom
		_zoom=value
		_mat=_mat.Scale( delta )
	End
	
	#Rem monkeydoc Create a new camera.
	
	zoom 		is (1,1).
	size 		is Display's system width/heigth.
	viewport 	is (0,0,1,1) - whole screen.
	
	If this is the first created camera, it will be assigned to Camera.main global variable with tag "MainCamera".
	(this tag will be added to TagManager automatically)
	#End
	Method New()
			
		_zoom=New Vec2f( 1,1 )
		
		_size=New Vec2f( Display.systemWidth,Display.systemHeight )
		
		viewport=New Rectf( 0,0,1,1 ) 'whole screen
				
		_aspect=_size.x/_size.y
		
		_cameras.Add( Self )
		
	End
	
	#Rem monkeydoc Transforms position from screen space into world space.
	#End
	Method ScreenToWorldPoint:Vec2f( position:Vec2f )
		
		position-=New Vec2f( _viewportPixels.Width,_viewportPixels.Height )*.5
		Return position-transform.localPosition
		
		'Local m:=transform.matrix
		'm.t=m.t*New Vec2f( 1,-1 ) 'invert Y
		'm=m*_mat
		'Local p:=AffineGetPosition( m )
		'p-=
		'Return position-p
	End
	
	#Rem monkeydoc Transforms mouse position into screen space.
	#End
	Method MouseToScreenPoint:Vec2f( position:Vec2f )
	
		position.y=_viewportPixels.Height-position.y
		Return position
	End
	
	#Rem monkeydoc Transforms position from world space into screen space.
	#End
	Method WorldToScreenPoint:Vec2f( position:Vec2f )
	
		Return position
	End
	
	#Rem monkeydoc Return stack iterator of all cameras in the scene.
	#End
	Function GetAllCameras:Stack<Camera>.Iterator()
		
		Return _cameras.All()
	End
	
	
	Protected
	
	Method Render( canvas:Canvas )
		
		canvas.Viewport=_viewportPixels
		
		canvas.Clear( backgroundColor )
		
		Local tm:=transform.matrix
		tm.t=tm.t*New Vec2f( 1,-1 )'*_renderZoom 'invert Y
		
		canvas.Matrix=_mat*tm
		
		ScriptExecutorBridge_Camera.ExecuteRender( canvas )
		
	End
	
	Method OnDestroy() Override
	
		_cameras.Remove( Self )
	End
	
	Method OnAttached() Override
	
		If main Return
		
		main=Self
		TagManager.RegisterTag( "MainCamera" )
		main.tag="MainCamera"
	End
	
	Function RenderAll( canvas:Canvas )
		
		canvas.Clear( Color.Black )
		
		For Local cam:=Eachin _cameras
			
			If Not cam.isActiveAndEnabled Continue
			
			Local v:=canvas.Viewport
			Local c:=canvas.Color
			Local m:=canvas.Matrix
			Local a:=canvas.Alpha
			
			cam.Render( canvas )
			
			canvas.Viewport=v
			canvas.Color=c
			canvas.Matrix=m
			canvas.Alpha=a
		Next
		
	End
	
	
	Private
	
	Field _mat:=New AffineMat3f
	Field _viewport:Rectf,_viewportPixels:Rectf
	Field _size:Vec2f
	Field _zoom:Vec2f
	Field _aspect:Float
	Field _renderZoom:Vec2f
	
	Global _cameras:=New Stack<Camera>
	
End


Private

Class ScriptExecutorBridge_Camera Extends ScriptExecutor Abstract

	Function ExecuteRender( canvas:Canvas )
		ScriptExecutor.ExecuteRender( canvas )
	End
	
End
