
Namespace cluqua2d.demo


#Import "assets/"

#Import "cluqua2d/cluqua2d"
'#Import "<cluqua2d>"
#Import "<std>"
#Import "<mojo>"
#Import "<reflection>"

Using std..
Using mojo..
'Using cluqua2d..


' app template
Function Main()
	
	New CluquaApplication
	Application.Init( "cluqua2d - Big Wheel demo",800,800 )
	
	Application.Initialized=OnInitializeApp
	'Application.RenderLoadingCallback=OnRenderLoading
	
	Application.Run()
End


Class TestClass
	
	Field data:=0
	
	Operator To:String()
		Return "+"+data+"+"
	End
End

Class TestClass Extension
	
	Method PrintMe()
		If Self = Null Then Print "+null+" ; Return
		Print Self
	End
End

' entrance point for all our code
Function OnInitializeApp()
	
	#Rem
	Local create:=Lambda:TestClass()
		Return New TestClass
	End
	Local reset:=Lambda(obj:TestClass)
		obj.data=-1
	End
	Local pool:=New ObjectPool<TestClass>( 2,0,create,reset )
	pool.Acquire().PrintMe()
	pool.Acquire(False).PrintMe()
	pool.Acquire().PrintMe()
	#End
	
	' all gameobject's tags must be registered before use
	TagManager.RegisterTag( "Base" )
	TagManager.RegisterTag( "Cab" )
	TagManager.RegisterTag( "TopCamera" )
	
	Local scene:=SceneManager.NewScene( "MainScene" )
	
	Local camObj:=CreateCamera( "Camera" )
	camObj.tag="MainCamera"
	'camObj.transform.localPosition=New Vec2f( 0,Display.halfSystemHeight )
	scene.AddChild( camObj )
	Local cam:=camObj.GetComponent<Camera>()
	cam.backgroundColor=Color.Sky
	'cam.viewport=New Rectf( 0,0,0.75,0.75 )
	
	camObj=CreateCamera( "Camera2" )
	camObj.tag="TopCamera"
	scene.AddChild( camObj )
	cam=camObj.GetComponent<Camera>()
	cam.viewport=New Rectf( 0.66,0.66,1,1 )
	cam.zoom=New Vec2f( .5,.5 )
	cam.enabled=False 'is off
	
	Local goWheel:=New GameObject( "TheWheel" )
	scene.AddChild( goWheel ) 'directly attach to scene only this object
	goWheel.AddComponent<MainLogic>() 'check for key.escape here
	
	Local tf:=goWheel.transform
	'tf.localPosition=New Vec2f( Display.halfWidth,Display.height ) 'display hcenter|bottom
	
	'Return
	
	Local size:Vec2i
	Local sprite:Sprite
	Local go:GameObject
	
	' base
	sprite=SpriteManager.CreateSprite( "BaseSprite","base.png",Null,Anchor.HCenterBottom )
	go=CreateSprite( "Base",sprite )
	go.SetParent( tf )
	go.transform.localPosition=New Vec2f
	go.tag="Base"
	
	'Return
	
	' circle
	sprite=SpriteManager.CreateSprite( "CircleSprite","circle.png",Null,Anchor.HCenterVCenter )
	go=CreateSprite( "Circle",sprite )
	go.SetParent( tf )
	go.transform.localPosition=New Vec2f( 0,445 )
	Local rotator:=go.AddComponent<Rotator>()
	rotator.rotSpeed=5
	'sprite.RemoveComponent<Rotator>()
	
	'Return
	
	'cabins
	sprite=SpriteManager.CreateSprite( "CabinSprite","cabin.png",Null,Anchor.HCenterVCenter )
	sprite.pivot=New Vec2f( sprite.width*.5,11 ) 'hcenter|custom
	tf=go.transform 'will attach to circle now
	Local count:=10
	Local delta:=360.0/Float(count)
	Local start:=Rnd( 360 ) 'starts with random shift
	Local radius:=604.0*.5
	For Local i:=0 Until count
		go=CreateSprite( "Cab"+(i+1),sprite )
		go.tag="Cab"
		go.SetParent( tf )
		Local a:= ToRadians( start+i*delta )
		go.transform.localPosition=New Vec2f( radius*Cos(a),radius*Sin(a) )
		Local fixedRotator:=go.AddComponent<FixedRotator>()
	Next
	
	' find via transform
	Local cab:=goWheel.transform.Find( "Circle/Cab5" ).gameObject
	Local rend:=cab.GetComponent<SpriteRenderer>()
	rend.color=Color.Red
	
	cab=goWheel.transform.Find( "Circle/Cab1" ).gameObject
	cab.transform.localScale=New Vec2f( .5,.5 ) 'make it half smaller
	
	' find via gameobject
	cab=GameObject.Find("TheWheel/Circle/Cab10" )
	rend=cab.GetComponent<SpriteRenderer>()
	rend.color=Color.DarkGrey
	
	' find via tag
	go=GameObject.FindWithTag("Base" )
	rend=go.GetComponent<SpriteRenderer>()
	rend.color=Color.Orange
	
	Local goArr:=GameObject.FindAllWithTag("Cab" )
	For Local i:=Eachin goArr
		If i.name<>"Cab1"
			rend=i.GetComponent<SpriteRenderer>()
			rend.alpha=Rnd( 0.2,0.8 )
			'rend.enabled=False
		Endif
	Next
	
End

'Function OnRenderLoading( canvas:Canvas )
'End


' our game logics


Class FixedRotator Extends MonkeyBehaviour Implements IBehStart,IBehLateUpdate
	
	Field fixedAngle:=0.0
	Field target:Transform
	
	
	Private
	
	Method Start()
		
		If Not target Then target=transform
	End
	
	Method LateUpdate()
		
		target.rotation=fixedAngle
	End
	
End


Class Rotator Extends MonkeyBehaviour Implements IBehStart,IBehUpdate

	Field rotSpeed:=0.0
	Field target:Transform
	
	
	Private
	
	Method Start()
	
		If Not target Then target=transform
	End
	
	Method Update()
		
		target.Rotate( rotSpeed*Timing.deltaTime )
		
	End
		
End


Class MainLogic Extends MonkeyBehaviour Implements IBehUpdate,IBehStart

	
	Private
	
	Field _speed:=200.0
	Field _startPos:Vec2f
	
	Method Start()
	
		_startPos=transform.localPosition
		gameObject.AddComponent<MyRenderer>() 'will use our custom renderer
	End
	
	'Method Render( canvas:Canvas )
		
		'canvas.DrawOval( -5,-5,10,10 )
		'canvas.DrawLine( Display.halfWidth,0,Display.halfWidth,Display.height )
		'canvas.DrawLine( 0,Display.halfHeight,Display.width,Display.halfHeight )
	'End
	
	Method Update()
	
		
		If Mouse.ButtonHit( MouseButton.Left )
			
			Local cam:=Camera.main
			Local pos:=Mouse.Location
			Local pos1:=cam.MouseToScreenPoint( pos )
			Local pos2:=cam.ScreenToWorldPoint( pos1 )
			Print "screen: "+pos1+", world: "+pos2+", tform: "+cam.transform.localPosition
			'Print "world: "+pos2+", screen: "+Camera.main.WorldToScreenPoint( pos2 )
			
		Endif
	
	
		Local shift:=(Keyboard.Modifiers & Modifier.Shift)
		
		
		If Keyboard.KeyReleased( Key.Escape )
			
			App.Terminate()

		Elseif Keyboard.KeyDown( Key.Left )
			
			Local tf:= shift ? Camera.main.transform Else transform
			tf.Translate( New Vec2f( -_speed*Timing.deltaTime,0 ) )
		
		Elseif Keyboard.KeyDown( Key.Right )
			
			Local tf:= shift ? Camera.main.transform Else transform
			tf.Translate( New Vec2f( _speed*Timing.deltaTime,0 ) )
		
		Elseif Keyboard.KeyDown( Key.Up )
		
			Local tf:= shift ? Camera.main.transform Else transform
			tf.Translate( New Vec2f( 0,_speed*Timing.deltaTime ) )
		
		Elseif Keyboard.KeyDown( Key.Down )
		
			Local tf:= shift ? Camera.main.transform Else transform
			tf.Translate( New Vec2f( 0,-_speed*Timing.deltaTime ) )
		
		Elseif Keyboard.KeyDown( Key.Q )
		
			Local tf:= shift ? Camera.main.transform Else transform
			tf.Rotate( -15.0*Timing.deltaTime )
			
		Elseif Keyboard.KeyDown( Key.W )
		
			Local tf:= shift ? Camera.main.transform Else transform
			tf.Rotate( 15.0*Timing.deltaTime )
			
		Elseif Keyboard.KeyDown( Key.Z )
		
			Local sc:=transform.localScale
			sc-=(New Vec2f( 1,1 ))*Timing.deltaTime
			
			Local tf:= shift ? Camera.main.transform Else transform
			tf.localScale=sc
		
		Elseif Keyboard.KeyDown( Key.X )
		
			Local sc:=transform.localScale
			sc+=(New Vec2f( 1,1 ))*Timing.deltaTime
			
			Local tf:= shift ? Camera.main.transform Else transform
			tf.localScale=sc
		
		Elseif Keyboard.KeyReleased( Key.R ) 'reset transform
		
			Local tf:= shift ? Camera.main.transform Else transform
			tf.Reset()
			tf.localPosition=_startPos
		
		Elseif Keyboard.KeyReleased( Key.D ) 'delete second camera
		
			Local go:=GameObject.FindWithTag( "TopCamera" )
			If go
				go.Destroy()
			Endif
		
		Elseif Keyboard.KeyReleased( Key.C ) 'enable/disable second camera
		
			Local go:=GameObject.FindWithTag( "TopCamera" )
			If go
				Local cam:=go.GetComponent<Camera>()
				cam.enabled=Not cam.enabled
			Endif
		
		Elseif Keyboard.KeyReleased( Key.T ) 'camera zoom
		
			Local cam:=Camera.main
			Local z:=cam.zoom
			z = z.x > 1.1 ? New Vec2f( 1,1 ) Else New Vec2f( 2,2 )
			cam.zoom=z
			
		Endif
	End
	
	
	Class MyRenderer Extends Renderer
		
		Method Render( canvas:Canvas ) Override
		
			canvas.DrawOval( -5,-5,10,10 )
			'canvas.DrawLine( Display.halfWidth,0,Display.halfWidth,Display.height )
			'canvas.DrawLine( 0,Display.halfHeight,Display.width,Display.halfHeight )
		End
	End
End
