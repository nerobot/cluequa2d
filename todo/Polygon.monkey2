
Namespace cluqua2d


#Rem monkeydoc 
#End
Class Polygon

	#Rem monkeydoc 
	#End
	Method New( xyArray:Float[] )
	
		_points=New Float[xyArray.Length]
		For Local k:=0 Until xyArray.Length
			_points[k]=xyArray[k]
		Next
		UpdateBounds()
	End
	
	#Rem monkeydoc 
	#End
	Method New( rect:Rectf )
	
		_points=New Float[8]
		_points[0]=rect.Left
		_points[1]=rect.Top
		_points[2]=rect.Right
		_points[3]=rect.Top
		_points[4]=rect.Right
		_points[5]=rect.Bottom
		_points[6]=rect.Left
		_points[7]=rect.Bottom
		_bounds=rect
	End
	
	#Rem monkeydoc 
	#End
	Property Points:Float[]()
		Return _points
	End
	
	#Rem monkeydoc 
	#End
	Property Bounds:Rectf()
		Return _bounds
	End
	
	#Rem monkeydoc 
	#End
	Property Origin:Vec2f()
		Return _origin
	Setter( value:Vec2f )
		_origin=value
	End
	
	#Rem monkeydoc 
	#End
	Property Rotation:Float()
		Return _rotation
	End
	
	#Rem monkeydoc 
	#End
	Method Rotate( angle:Float )
		
		Local ox:=_origin.x
		Local oy:=_origin.y
		Local cos:=Cos( angle )
		Local sin:=Sin( angle )
		
		For Local k:=0 Until _points.Length Step 2
			Local px:=_points[k]-ox
			Local py:=_points[k+1]-oy
			_points[k]=ox + px*cos - py*sin
			_points[k+1]=oy + px*sin + py*cos
		Next
		_rotation=(_rotation + angle) Mod 360.0
		UpdateBounds()
	End
	
	#Rem monkeydoc 
	#End
	Method Move( dx:Float,dy:Float )
		
		For Local k:=0 Until _points.Length Step 2
			_points[k]+=dx
			_points[k+1]+=dy
		Next
		Local v:=New Vec2f( dx,dy )
		_bounds+=v
		_origin+=v
	End
	
	#Rem monkeydoc 
	#End
	Method PointIn:Bool( point:Vec2f )
		
		If Not _bounds.Contains( point ) Return False
		Return PointIn( _points,point )
	End
	
	#Rem monkeydoc 
	#End
	Method PointIn:Bool( px:Float,py:Float )
		
		Return PointIn( New Vec2f( px,py ) )
	End
	
	#Rem monkeydoc 
	#End
	Function PointIn:Bool( xy:Float[],point:Vec2f )
		
		Local px:=point.x,py:=point.y
		Local x1:Float,y1:Float,x2:Float,y2:Float
		Local koef:Float,koefA:Float
		Local cnt:Int=xy.Length
		Local sign:Int,sign2:Int
		
		For Local k:=0 Until cnt Step 2
			x1=xy[k]
			y1=xy[k + 1]
			If k + 3 < cnt
				x2=xy[k + 2]
				y2=xy[k + 3]
			Else 'connect with first point
				x2=xy[0]
				y2=xy[1]
			Endif
			
			sign=Sgn( x2-x1 )
			sign2=Sgn( px-x1 )
			
			'prevent division by zero
			If sign=0
				x2+=0.0001
				sign=1
			Endif
			If sign2=0
				px+=0.0001
				sign2=1
			EndIf
			
			'the slope coefficient (y=k*x + b)
			koef=(y1 - y2) / (x2 - x1)
			koefA=(y1 - py) / (px - x1)
			
			'checking
			If (sign2=sign And koefA > koef) Or (sign2 <> sign And koefA < koef)
				Return False
			Endif
		
		Next
		
		Return True
		
	End Function
	
	
	Private
	
	Field _points:Float[] 'xy pairs in one dimention array
	Field _bounds:Rectf 'bounds used for fast collision detection
	Field _origin:=New Vec2f(0,0)
	Field _rotation:=0.0
	
	Method UpdateBounds()
	
		Local minX:=_points[0]
		Local maxX:=_points[0]
		Local minY:=_points[1]
		Local maxY:=_points[1]
		For Local k:=2 Until _points.Length Step 2
			Local px:=_points[k]
			Local py:=_points[k+1]
			If px < minX
				minX=px
			Elseif px > maxX
				maxX=px
			Endif
			If py < minY
				minY=py
			Elseif py > maxY
				maxY=py
			Endif
		Next
		If _bounds=Null Then _bounds=New Rectf
		_bounds.Left=minX
		_bounds.Right=maxX
		_bounds.Top=minY
		_bounds.Bottom=maxY
	End
	
End
