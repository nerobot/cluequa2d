
Namespace cluqua2d


#Rem monkeydoc 
#End
Class PolygonRenderer Extends Renderer
	
	#Rem monkeydoc 
	#End
	Field poly:Polygon
	
	
	Protected
	
	Method Render( canvas:Canvas ) Override
		
		If Not poly Return
		
		Local pos:=transform.localPosition
		canvas.Translate( pos.x,pos.y )
		
		canvas.Color=color
		canvas.DrawPoly( poly.Points )
		
		canvas.Color *= 0.25
		canvas.DrawRect( poly.Bounds )
		
		canvas.Color=Color.Black
		
		canvas.DrawOval( poly.Origin.x-2,poly.Origin.y-2,4,4)
		
	End
	
End
