
Namespace cluqua2d


#Rem monkeydoc 
#End
Class RectRenderer Extends Renderer
	
	#Rem monkeydoc 
	#End
	Field width:Float
	
	#Rem monkeydoc 
	#End
	Field height:Float
	
	#Rem monkeydoc 
	#End
	Method SetSize( w:Float,h:Float )
		width=w
		height=h
	End
	
	
	Protected
	
	Method Render( canvas:Canvas ) Override
	
		'canvas.Color = color
		
		'Local pos := transform.position;
		'Local sc := transform.scale;
		'Print "rect.pos: "+pos.x+","+pos.y+" - "+transform.localPosition.x+","+transform.localPosition.y
		'Scale(sc.x, sc.y)
		'DrawRect(0, 0, width, height)
	End
	
End
